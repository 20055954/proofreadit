###Application Info###

* Name: **ProofreadIt!**
* Author: Andrey Shevyakov
* Version: 1.0b
* Description: **ProofreadIt!** is an online collaborative proofreading platform for the creative fiction writers.

###Next Version Plans###
The **1.0** version will include the email-verification of newly registered users, in order to prevent spamming and other types of illegitimate use.