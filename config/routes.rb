Rails.application.routes.draw do

resources :users        
resources :texts
resources :tags
resources :comments
resources :feedbacks
resources :ratings

root 'static_pages#home'

get 'help'    => 'static_pages#help'
get 'about'   => 'static_pages#about'
get 'tc' => 'static_pages#terms_and_conditions'
get 'contact' => 'static_pages#contact'
get 'signup'  => 'users#new'
get    'login'   => 'sessions#new'
post   'login'   => 'sessions#create'
delete 'logout'  => 'sessions#destroy'
get 'new_text' => 'texts#new'
delete 'remove_text' => 'texts#destroy'
get 'lock_text' => 'texts#lock'
get 'unlock_text' => 'texts#unlock'
get 'search_by_tag' => 'tags#show'
get'list_texts' => 'texts#bytag'
post 'leave_comment' => 'texts#comment'
get 'leave_feedback' => 'feedbacks#new'
get 'bad_show' => 'texts#unsatisfactory_proofreading'
get 'good_show' => 'texts#satisfactory_proofreading'
get 'rate_text' => 'texts#rate'
get 'search' => 'texts#search'

end
