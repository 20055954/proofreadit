class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :desc
	  t.integer :user_id
	  t.string :email
      t.boolean :addressed, default: false

      t.timestamps null: false
    end
  end
end
