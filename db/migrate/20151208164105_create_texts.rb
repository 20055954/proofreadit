class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.integer :author
      t.integer :proofreader
      t.string :tags
      t.text :content
      t.text :corrected_content
      t.boolean :locked, default: false
      t.boolean :proofread_rated, default: false
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
