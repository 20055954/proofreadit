class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :ctext
      t.integer :commenter
      t.text :content
     
      t.belongs_to :user, index: true
      t.belongs_to :text, index: true

      t.timestamps null: false
    end
  end
end
