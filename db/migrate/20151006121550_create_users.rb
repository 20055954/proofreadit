class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :description
      t.date :dob
      t.string :gender
      t.string :email
      t.integer :tokens, default: 1
      t.integer :strikes, default: 0
      t.date :banned
      
      t.timestamps null: false
    end
  end
end
