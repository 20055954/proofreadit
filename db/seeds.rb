# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

joe = User.create(name: "Joe Bloggs", description: "Sci-Fi Writer", gender: "Male", dob: "1990-12-01", email: "joebloggs@mail.com", password: "secret", password_confirmation: "secret") 
ethan = User.create(name: "Ethan Brook", description: "Comedy Writer", gender: "Male", dob: "1960-06-12", email: "ethan500fun@mail.com", password: "ethan3134", password_confirmation: "ethan3134") 
charlotte = User.create(name: "Charlotte Brunowski", description: "Crime Writer", gender: "Female", dob: "1989-11-11", email: "charile2222sx@gmail.com", password: "sec123", password_confirmation: "sec123") 

Tag.create(name:"comedy")
Tag.create(name:"fun")
Tag.create(name:"crime")
Tag.create(name:"aliens")

t1 = Text.create(author: joe.id, tags: "aliens", content: "One day i was walking down the street, I saw a bunch of aliens, they started shooting at me and I died due to multiple injueires. And that was Sci-Fi, all hail Carl Sagan!",
corrected_content: "One day i was walking down the street, I saw a bunch of aliens, they started shooting at me and I died due to multiple injueires. And that was Sci-Fi, all hail Carl Sagan!")
t2 = Text.create(author: ethan.id, proofreader: charlotte.id, tags: "fun,comedy", content: "One day I told a funny joke about a chair.\nEveryone in Kazakhstan laughed.\nWas not funny in Englands, thoughf",
corrected_content: "One day I told a funny joke about a chair.\nEveryone in Kazakhstan laughed.\nWas not funny in England, though")
t3 = Text.create(author: charlotte.id, tags: "crime", content: "One day I witnessed a crime. It was a crimeous crime. The crimiest crime I have ever witnessed. I hope I will never have to go through this again!")

Comment.create(ctext: t1.id, commenter: ethan.id, content: "Spectacular!")
Comment.create(ctext: t1.id, commenter: charlotte.id, content: "Stupid...")
Comment.create(ctext: t2.id, commenter: joe.id, content: "LOLed so hard!!!")
Comment.create(ctext: t3.id, commenter: ethan.id, content: "Didn't get it.")

