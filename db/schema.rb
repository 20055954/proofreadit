# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160113124307) do

  create_table "comments", force: :cascade do |t|
    t.integer  "ctext"
    t.integer  "commenter"
    t.text     "content"
    t.integer  "user_id"
    t.integer  "text_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["text_id"], name: "index_comments_on_text_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "feedbacks", force: :cascade do |t|
    t.string   "desc"
    t.integer  "user_id"
    t.string   "email"
    t.boolean  "addressed",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "rating"
    t.integer  "user_id"
    t.integer  "text_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true

  create_table "texts", force: :cascade do |t|
    t.integer  "author"
    t.integer  "proofreader"
    t.string   "tags"
    t.text     "content"
    t.text     "corrected_content"
    t.boolean  "locked",            default: false
    t.boolean  "proofread_rated",   default: false
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "texts", ["user_id"], name: "index_texts_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.date     "dob"
    t.string   "gender"
    t.string   "email"
    t.integer  "tokens",          default: 1
    t.integer  "strikes",         default: 0
    t.date     "banned"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "password_digest"
    t.string   "remember_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
