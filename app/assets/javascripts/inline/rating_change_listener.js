$(document).ready(function () {

$(function() {
  $(".star")
    .mouseover(function() { 
	   for (i = 0; i < 5; i++)
	   {
		   $($("#rating-bar").children()[i]).attr("src", $("#star-unselected-path").text());
		   $($("#rating-bar").children()[i]).attr("class", "star unrated");
	   }	
	   for (i = 0; i <= $(this).index(); i++)
	   {
		   $($("#rating-bar").children()[i]).attr("src", $("#star-selected-glow-path").text());
		   $($("#rating-bar").children()[i]).attr("class", "star rated");
	   }
    })
    .mouseout(function() {
	   var avg = parseInt($("#avg-val").text());
	   for (i = 0; i < 5; i++)
	   {
		   if (i < avg) 
		   {
			   $($("#rating-bar").children()[i]).attr("src", $("#star-selected-path").text());
			   $($("#rating-bar").children()[i]).attr("class", "star rated");
		   }
		   else 
		   {
			   $($("#rating-bar").children()[i]).attr("src", $("#star-unselected-path").text());
			   $($("#rating-bar").children()[i]).attr("class", "star unrated");
		   }
	   }
    });
 });
 
$(".star")
	.click(function() {
		var index = $(this).index() + 1;
		$.get('/rate_text', { text_id: $("#text-id-val").text(), rating: index});
	});
});
