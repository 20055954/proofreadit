# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

root = exports ? this

root.show_original_text = (orig) -> 
        $("#curc").val($("#text_corrected_content").val())
        $("#show-text").attr("onclick", "show_corrected_text($('#curc').val())")
        $("#text-change-btn").attr("disabled", "disabled")
        $("#undo-text").attr("disabled", "disabled")
        $("#highlight-text").attr("disabled", "disabled")
        $("#show-text").text("Show Corrected Text")
        $("#content").text("Original Text")
        $("#text_corrected_content").val(orig)

root.show_corrected_text = (curc) ->
        $("#show-text").attr("onclick", "show_original_text($('#orig').val())")
        $("#text-change-btn").attr("disabled", false) if curc != $("#orig").val() and curc != $("#corr").val()
        $("#undo-text").attr("disabled", false)
        $("#highlight-text").attr("disabled", false)
        $("#show-text").text("Show Original Text")
        $("#content").text("Text")
        $("#text_corrected_content").val(curc)

root.undo_current_changes = (corr) ->
        $("#text_corrected_content").val(corr)
        $("#text-change-btn").attr("disabled", "disabled")

root.highlight_differences = (corr, orig) ->

        if $("#highlight-explanations").css("display") == "none" 
                $("#curc").val($("#text_corrected_content").val())

        cur_state = $("#curc").val()

        $("#highlight-explanations").toggle()

        if $("#highlight-text").attr("data-state") == "on"
                $("#highlight-text").text("Highlight Corrections Made")
                $("#highlight-text").attr("data-state", "off")
                $("#text-change-btn").attr("disabled", false) if cur_state != orig and cur_state != corr
                $("#undo-text").attr("disabled", false)
                $("#show-text").attr("disabled", false)
                $("#content").text("Text")
                $("#text_corrected_content").val(cur_state)
                $('#text_corrected_content').highlightTextarea('disable')
                $("#text_corrected_content").attr("disabled", false)

        else

                $("#content").text("Corrections Made")
                $("#highlight-text").text("Return To Editing")
                $("#highlight-text").attr("data-state", "on")
                $("#text-change-btn").attr("disabled", "disabled")
                $("#undo-text").attr("disabled", "disabled")
                $("#show-text").attr("disabled", "disabled")

                orig_lines = orig.split("\n")
                corr_lines = corr.split("\n")

                orig_lines_w_content = []
                corr_lines_w_content = []

                for line, i in orig_lines
                        if line.trim() != ""
                                orig_lines_w_content.push([i, line])

                for line, i in corr_lines
                        if line.trim() != ""
                                corr_lines_w_content.push([i, line])

                highlight_changed = []
                highlight_added = []
                highlight_removed = []

                index = 1
                start = 0

                if orig_lines_w_content.length <= corr_lines_w_content.length

                        for line, i in corr_lines_w_content
                                try
                                        if line[1].toString().toLowerCase() !=  orig_lines_w_content[i][1].toString().toLowerCase()
                                                highlight_changed.push([corr_lines_w_content[i][0], corr_lines_w_content[i][1].length])
                                catch error

                        for line, i in corr_lines_w_content
                                if i > orig_lines_w_content.length - 1
                                        highlight_added.push([corr_lines_w_content[i][0], corr_lines_w_content[i][1].length])  

                        while corr.indexOf("\n",start) != -1
                                for pos in highlight_changed
                                        if pos[0] == index
                                                pos[0] = corr.indexOf("\n", start)
                                                pos[1] = corr.indexOf("\n", start) + pos[1] + 1
                                for pos in highlight_added
                                        if pos[0] == index
                                                pos[0] = corr.indexOf("\n", start)
                                                pos[1] = corr.indexOf("\n", start) + pos[1] + 1
                                start = corr.indexOf("\n", start) + 1
                                index++

                        $("#text_corrected_content").val(corr)

                else

                        for line, i in orig_lines_w_content
                                try
                                        if line[1].toString().toLowerCase() !=  corr_lines_w_content[i][1].toString().toLowerCase()
                                                highlight_changed.push([orig_lines_w_content[i][0], orig_lines_w_content[i][1].length])
                                catch error

                        for line, i in orig_lines_w_content
                                if i > corr_lines_w_content.length - 1
                                        highlight_removed.push([orig_lines_w_content[i][0], orig_lines_w_content[i][1].length])  

                        while orig.indexOf("\n",start) != -1
                                for pos in highlight_changed
                                        if pos[0] == index
                                                pos[0] = orig.indexOf("\n", start)
                                                pos[1] = orig.indexOf("\n", start) + pos[1] + 1
                                for pos in highlight_removed
                                        if pos[0] == index
                                                pos[0] = orig.indexOf("\n", start)
                                                pos[1] = orig.indexOf("\n", start) + pos[1] + 1
                                start = orig.indexOf("\n", start) + 1
                                index++

                        $("#text_corrected_content").val(orig)

                $('#text_corrected_content').highlightTextarea({
                ranges: [{
                color: 'orange',
                ranges: highlight_changed
                }, {
                color: '#76EE00',
                ranges: highlight_added
                }, {
                color: 'red',
                ranges: highlight_removed
                }] 
                })

                $('#text_corrected_content').highlightTextarea('enable')
                $("#text_corrected_content").attr("disabled", "disabled")
