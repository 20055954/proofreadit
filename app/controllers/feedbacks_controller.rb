class FeedbacksController < ApplicationController

	def index
		redirect_to leave_feedback_path
	end

	def new
		@feedback = Feedback.new
	end

	def create
		secure_params = params.require(:feedback).permit(:desc, :user_id, :addressed, :email)
		
		if logged_in?
			secure_params[:user_id] = current_user.id
		end
		
		@feedback = Feedback.new(secure_params)
		
		if @feedback.save
		
			if logged_in?
				Feedback.find(@feedback.id).update_attribute :email, nil
			end
			
			flash[:success] = "Feedback Submitted"  
			redirect_to root_path
		else
			render 'new'
		end
	end

end
