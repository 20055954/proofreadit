class TextsController < ApplicationController

	helper_method :restrict_access
	helper_method :restrict_access_banned
	before_action :restrict_access
	before_action :restrict_access_banned
	
	def index
		redirect_to root_path
	end

	def new
		@text = Text.new
	end

	def show
		@text = Text.find(params[:id])
		
		begin
			@all_ratings = @text.ratings
			@first = true
			@avg = 0
			@all_ratings.each do |r| 
				@avg = @avg + r.rating
				if @first == true
					@first = false
				else
					@avg = @avg / 2
				end
			end
			rescue
			if @all_ratings == nil
				@avg = 0
			else 
				@avg = @all_ratings.rating
			end
		ensure
		end
		
		@avg = @avg.round

		@comments = @text.comments
	end

	def edit
		@text = Text.find(params[:id])
	end

	def comment
		secure_params = params.require(:comment).permit(:ctext, :commenter, :content)
		@comment = Comment.new(secure_params)
		
		if @comment.save then
			flash[:success] = "Commented" 
		else
			flash[:error] = "Can't Leave Blank Comments"
		end
		
		redirect_to text_path(Text.find(secure_params[:ctext])) + "#comment-header"
	end

	def bytag
		
		@tag = params[:tagname]
		@result = []
		@texts = Text.find_each do |t|
			t.tags.split(",").each do |tt|
				if (tt == @tag) then 
					@result.push t 
				end
			end
		end
	end

	def update
		
		@text = Text.find(params[:id])
		corr_c = @text.corrected_content
		@comments = @text.comments
		secure_params = params.require(:text).permit(:corrected_content, :proofreader)
		secure_params[:proofreader] = current_user.id
		if @text.update_attributes(secure_params)
			flash[:success] = "Text Updated"
			@text.update_attribute(:proofread_rated, false)
			redirect_to @text
		else
			@text.corrected_content = corr_c
			render 'show'
		end
	end

	def create
		
		# Should Delete Last Author - Did
		secure_params = params.require(:text).permit(:author, :tags, :content)
		secure_params[:author] = current_user.id
		
		@user = User.find(secure_params[:author])
		
		if @user.tokens != 0
			tags = secure_params[:tags].split(",")
				
			tags_in = ""
			index = 0
			
			tags.each do |t|
			
				t = t.strip.downcase
				
				if index != 0 
					tags_in = tags_in + "," + t 
				else
					tags_in = t
				end
				
				index+= 1
				
			end
			
			secure_params[:tags] = tags_in

			begin
				tags.each do |t|
					t = t.strip.downcase
					tp = params.permit(:name)
					tp[:name] = t
					tag = Tag.new(tp)
					tag.save
			end

			rescue
			ensure
			end

			secure_params[:corrected_content] = secure_params[:content]
			@text = Text.new(secure_params)
			if @text.save
				@user.update_attribute(:tokens, @user.tokens - 1)
				flash[:success] = "Text Submited"  
				redirect_to @text  
			else
				render 'new'
			end
			
		else
			flash[:error] = "Not Enough Tokens"
			redirect_to current_user
		end
		
	end
	
	def destroy
		
		@text = Text.find(params[:id])
		
		if @text.author == current_user.id 
			Text.find(params[:id]).destroy
			flash[:success] = "Text Deleted"
			redirect_to current_user
		else
			redirect_to root_path
		end
	end
	
	def lock
		@text = Text.find(params[:id])
		
		if @text.author == current_user.id
			Text.find(params[:id]).update_attribute(:locked, true)
			flash[:success] = "Text Locked"
			redirect_to @text
		else
			redirect_to root_path
		end
	end
	
	def unlock
		@text = Text.find(params[:id])
		
		if @text.author == current_user.id
			Text.find(params[:id]).update_attribute(:locked, false)
			flash[:success] = "Text Unlocked"
			redirect_to @text
		else
			redirect_to root_path
		end
	end
	
	def satisfactory_proofreading
		@reader = User.find(params[:reader_id])
		
		if @reader.strikes > 0
			User.find(@reader.id).update_attribute(:strikes, @reader.strikes - 1)
		end
		
		@reader.update_attribute(:tokens, @reader.tokens + 1)
		
		Text.find(params[:text_id]).update_attribute(:proofread_rated, true)
		
		flash[:success] = "Rated"
		redirect_to Text.find(params[:text_id])
	end
	
	def unsatisfactory_proofreading
		@reader = User.find(params[:reader_id])
		
		if @reader.strikes < 3
			@reader.update_attribute(:strikes, @reader.strikes + 1)
		end
		
		if @reader.strikes == 3
			@reader.update_attribute(:banned, Date.today)
		end
		
		Text.find(params[:text_id]).update_attribute(:proofread_rated, true)
		
		flash[:success] = "Rated"
		redirect_to Text.find(params[:text_id])
	end
	
	def rate
		secure_params = params.permit(:user_id, :text_id, :rating)
		@text = secure_params[:text_id]
		secure_params[:user_id] = current_user.id
		
		@rating = nil
		@rating = Rating.find_by_text_id_and_user_id(secure_params[:text_id], secure_params[:user_id])
		
		if @rating != nil
			Rating.find(@rating.id).destroy
		end
		
		@rating = Rating.new(secure_params)
		
		if @rating.save
			flash[:success] = "Rated!"
		else
			flash[:error] = "Something Went Wrong"
		end
		
		render js: "window.location = '#{@text}'"
	end
	
	def search
		@search_term = params[:search].gsub(/[^0-9a-z ]/i, '').downcase.strip.split(" ")
		@results = []
		@all_texts = Text.all
		
		@all_texts.each do |t| 
			@search_term.each do |s_word|
			 
				User.find(t.author).name.split(" ").each do |a_name|
					if a_name.casecmp(s_word).zero?
						@results << t
					end
				end
				
				t.tags.split(",").each do |tag_c|
					if tag_c.casecmp(s_word).zero?
						@results << t
					end
				end
				
				if t.corrected_content.downcase.include? s_word
					@results << t
				end 
				
				if t.content.downcase.include? s_word
					@results << t
				end
				
			end
		end
		
		@results = @results.uniq
	end
	
end
