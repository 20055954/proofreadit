class UsersController < ApplicationController

helper_method :restrict_access

def index
	redirect_to root_path
end

def new
	if logged_in? then
		flash[:warning] = "You are already registered" 
		redirect_to current_user
	else
		@user = User.new
	end
end

def show
	restrict_access
	
	if current_user.banned != nil and current_user.banned < Date.today - 7.days
		User.find(current_user.id).update_attribute(:banned, nil)
		flash[:success] = "The Ban Period Is Over!"
		redirect_to current_user
	end
	
	@user = User.find(params[:id])
	
	if current_user.id != @user.id
		redirect_to root_path
	end
	
	@texts = @user.texts

	# Wishing User Happy Birthday
	if @user.dob.today? then 
		flash.now[:notice] = "Happy Birthday, #{@user.name}!"
	end 
end

def create
	secure_params = params.require(:user).permit(:name, :email, :dob, :description, :gender, 
	:password, :password_confirmation)
	@user = User.new(secure_params)
	if @user.save
		remember @user     
		flash.now[:success] = "Welcome to the ProofreadIt!"    
		redirect_to @user
	else
		render 'new' 
	end
end

end
