class TagsController < ApplicationController

helper_method :restrict_access
before_action :restrict_access

def show
	@tags = []
	Tag.find_each do |t|
		@tags.push t
	end
end

end
