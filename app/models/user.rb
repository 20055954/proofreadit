class User < ActiveRecord::Base
      attr_accessor :remember_token
      before_save { self.email = email.downcase }
      before_save { self.name = self.name.split.map(&:capitalize).join(' ') }
      validates :name, presence: true, length: { in: 4..70 }
      validates :description, presence: true, length: { in: 10..70 }
      VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
      validates :email, presence: true, 
                        format: { with: VALID_EMAIL_REGEX },
                        uniqueness: { case_sensitive: false }
      validates :password, presence: true, length: { minimum: 6 }
      validates :password_confirmation, presence: true
      validates :dob, presence: true, date: {after: Proc.new {Time.now - 100.years}, message: "must enter a valid date of birth"}
      validates :dob, date: {before: Proc.new {Date.today - 13.years}, message: "have to be at least 13 years old to use the service"}
      has_secure_password      # A magic method!!
      has_many :texts, :foreign_key => 'author'
      has_many :comments, :foreign_key => 'commenter'
      has_many :ratings, :foreign_key => 'user_id'
	  has_many :bugs

      # Returns the hash digest of a string.
      def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                      BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
      end

      # Returns a random token.
      def User.new_token
        SecureRandom.urlsafe_base64
      end

      # Remembers a user in the database for use in persistent sessions.
      def remember
        self.remember_token = User.new_token
        update_attribute(:remember_digest, User.digest(remember_token))
      end

      def authenticated?(remember_token)
        BCrypt::Password.new(remember_digest).is_password?(remember_token)
      end

       def forget
          update_attribute(:remember_digest, nil)
       end
end
