class Text < ActiveRecord::Base
	before_save { self.content = content.strip }
	validates :content, presence: true, :uniqueness => {:message => "was already submitted"}, length: { minimum: 100 }
	validates :corrected_content, length: { minimum: 100 }, :on => :update
	validates :tags, presence: true
	belongs_to :user
	has_many :comments, :foreign_key => 'ctext'
	has_many :ratings, :foreign_key => 'text_id'
end
