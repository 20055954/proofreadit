class Tag < ActiveRecord::Base
	before_save { self.name = name.downcase.strip }
	belongs_to :text
end
