class Feedback < ActiveRecord::Base
	belongs_to :user
	before_save { self.desc = desc.strip }
	before_save { self.email = email.downcase if self.email != nil}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true
    validates :email, format: { with: VALID_EMAIL_REGEX }
	validates :desc, presence: true, length: { minimum: 15, maximum: 1250 }
end
