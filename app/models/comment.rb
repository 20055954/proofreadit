class Comment < ActiveRecord::Base
    before_save { self.content = content.strip }
	belongs_to :text
	belongs_to :user
	validates :content, presence: true
end
